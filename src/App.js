import React, {Component} from 'react';
import Paper from "@material-ui/core/es/Paper/Paper";
import RegisterPage from "./pages/RegisterPage";
import withStyles from "@material-ui/core/styles/withStyles";
import {Typography} from "@material-ui/core";
import HomeIcon from "@material-ui/icons/Home";
import LoginPage from "./pages/LoginPage";
import LoginSuccessPage from "./pages/LoginSuccessPage";


class App extends Component {
  constructor() {
    super()
    this.state = {
      page: 'login',
      showIcon: true
    }
  }

  render() {
    const {classes} = this.props
    return (
      <div className={classes.root}>
        <Paper className={classes.container} style={{height: this.state.height}}>
          <HomeIcon onClick={() => this.setState({page: 'login'})} color="primary" aria-hidden={!this.state.showIcon}/>
          <div className={classes.header}>
            <img src={'logo.png'} className={classes.img}/>
            <Typography variant="h5" component="h3" className={classes.title}>
              Authentication Demo
            </Typography>
          </div>
          {
            this.state.page === 'register' && <RegisterPage
              onPageChange={page => this.setState({page: page})}
              onLinkClick={() => this.setState({showIcon: true, page: 'login'})}/>
          }
          {
            this.state.page === 'login' && <LoginPage
              onPageChange={page => this.setState({page: page})}
              onLinkClick={() => this.setState({ showIcon: true, page: 'register'})}/>
          }
          {
            this.state.page === 'loginSuccess' &&
            <LoginSuccessPage onPageChange={page => this.setState({showIcon: false, page: page})}/>
          }
        </Paper>
      </div>
    );
  }
}

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.divider,
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
    flex: 1,
    width: '100%',
    height: '100%',
    position: 'absolute'
  },
  container: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit,
    paddingBottom: theme.spacing.unit * 2,
    width: 330,
  },
  header: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    resizeMode: 'stretch'
  },
  title: {
    marginTop: theme.spacing.unit,
  }
});


export default withStyles(styles)(App);
