import React from 'react'
import withStyles from "@material-ui/core/es/styles/withStyles";
import HOTPLoginComponent from "../components/HOTPLoginComponent";
import HOTPRegisterComponent from "../components/HOTPRegisterComponent";


class HOTPPage extends React.PureComponent {
  constructor() {
    super()
    this.state = {
      username: '',
      page: 'login'
    }
  }

  handlerUsernameChange(event) {
    this.setState({
      username: event.target.value
    })
  }

  render() {
    const {classes} = this.props
    return (
      <div className={classes.container}>
        {
          this.state.page === 'login'
            ? <HOTPLoginComponent onLinkClick={() => this.setState({page: 'register'})}/>
            : <HOTPRegisterComponent onLinkClick={() => this.setState({page: 'login'})}/>
        }
      </div>
    )
  }
}

const styles = theme => ({
  container: {},
})

export default withStyles(styles)(HOTPPage)