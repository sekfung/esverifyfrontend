import React from 'react'
import {Button, TableBody, TableCell, TableRow, Typography} from "@material-ui/core";
import withStyles from "@material-ui/core/es/styles/withStyles";
import HomeIcon from '@material-ui/icons/Home'
import HomePage from "./HomePage";
import FIDO2Page from "./FIDO2Page";
import HOTPPage from "./HOTPPage";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import MenuItemComponent from "../components/MenuItemComponent";
import HOTPRegisterMenuComponent from "../components/HOTPRegisterMenuComponent";
import WebAuthn from "../utils/webauthn";
import httpRequest from "../utils/httpRequest";


class RegisterPage extends React.PureComponent {
  constructor() {
    super()
    this.state = {
      page: '',
      type: 'fido',
      username: '',
      return_digit: '6',
      secretKey: ''
    }
  }

  handlePageChange(page) {
    this.setState({page: page})
  }

  handlerUsernameChange(event) {
    this.setState({
      username: event.target.value
    })
  }

  submitRegForm() {
    if (this.state.type === 'fido') {
      this.submitFIDORegForm()
    }else {
      this.submitHOTPRegForm()
    }
  }

  submitHOTPRegForm() {
    httpRequest.post("/otpserverdemo/hotp/user/register", this.buildOTPForm())
      .then(response => {
        if (response) {
          if (response.errorCode === 0) {
            alert('Success')
            this.props.onPageChange('login')
          }else {
            alert(response.message)
          }
        }
      })
      .catch(err => {
        console.warn(err)
        alert(err)
      })
  }

  submitHOTPResetFrom() {
    httpRequest.post("/otpserverdemo/hotp/user/reset", this.buildOTPForm())
      .then(response => {
        if (response) {
          if (response.errorCode === 0) {
            alert('Success')
            this.props.onPageChange('login')
          }else {
            alert(response.message)
          }
        }
      })
      .catch(err => {
        console.warn(err)
        alert(err)
      })
  }


  buildOTPForm() {
    return JSON.stringify({
      username: this.state.username,
      returnDigit: Number(this.state.return_digit),
      secretKey: this.state.secretKey,
    })
  }

  async submitFIDORegForm() {
    WebAuthn.makeCredential(this.state.username.trim())
      .then(() => {
        this.setState({loggedIn: true})
        alert('Success')
        this.props.onPageChange('login')
      })
      .catch(err => {
        alert(err)
      })
  }

  render() {
    const {classes} = this.props
    return (
      <div className={classes.container}>
            <TextField
              id="outlined-name"
              label="username"
              className={classes.textField}
              value={this.state.username}
              onChange={event => this.handlerUsernameChange(event)}
              margin="normal"
            />
        <MenuItemComponent type={'fido'} onTypeChange={type => this.setState({type: type})}/>
        {
          this.state.type === 'hotp' && <HOTPRegisterMenuComponent onDigitChange={digit => this.setState({return_digit: digit})} onKeyChange={key => this.setState({secretKey: key})}/>
        }
        <div className={classes.buttonWrapper}>
          {
            this.state.type === 'hotp' &&  <Button color={"primary"} variant={"outlined"} className={classes.button} onClick={() => this.submitHOTPResetFrom()}>Reset</Button>

          }
          <Button color={"primary"} variant={"outlined"} className={classes.button} onClick={() => this.submitRegForm()}>Register</Button>
        </div>
          <div className={classes.registerWrapper}>
            <Link href={'javascript:;'} className={classes.tips} onClick={this.props.onLinkClick}>
              SIGN IN
            </Link>
          </div>
      </div>
    );
  }
}

const styles = theme => ({
  container: {
    height: '100%',
    flexDirection: 'column',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    paddingRight: theme.spacing.unit,
    marginTop: theme.spacing.unit

  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  button: {
    marginTop: 8,
    width: 200
  },
  buttonWrapper: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: theme.spacing.unit
  },
  registerWrapper: {
    marginTop: theme.spacing.unit,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  textField: {
    width: 220,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },

})

export default withStyles(styles)(RegisterPage)