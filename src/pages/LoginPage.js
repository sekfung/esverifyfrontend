import React from 'react'
import {Button, TableBody, TableCell, TableRow, Typography} from "@material-ui/core";
import withStyles from "@material-ui/core/es/styles/withStyles";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import MenuItemComponent from "../components/MenuItemComponent";
import HOTPRegisterMenuComponent from "../components/HOTPRegisterMenuComponent";
import WebAuthn from "../utils/webauthn";
import global from '../global'
import httpRequest from "../utils/httpRequest";
import HOTPLoginComponent from "../components/HOTPLoginComponent";
import HOTPLoginMenuComponent from "../components/HOTPLoginMenuComponent";


class LoginPage extends React.PureComponent {
  constructor() {
    super()
    this.state = {
      page: '',
      type: 'fido',
      username: '',
      code: '',
    }
  }

  handlePageChange(page) {
    this.setState({page: page})
  }

  handlerUsernameChange(event) {
    this.setState({
      username: event.target.value
    })
  }

  submitLoginForm() {
    if (this.state.type === 'fido') {
      this.submitFIDOLoginForm()
    } else {
      this.submitHOTPLoginForm()
    }
  }

  submitHOTPLoginForm() {
    httpRequest.post("/otpserverdemo/hotp/user/login", this.buildOTPForm())
      .then(response => {
        if (response) {
          if (response.errorCode === 0) {
            alert('Success')
            this.props.onPageChange('loginSuccess')
          } else {
            alert(response.message)
          }
        }
      })
      .catch(err => {
        console.warn(err)
        alert(err)
      })
  }


  buildOTPForm() {
    return JSON.stringify({
      username: this.state.username,
      code: this.state.code
    })
  }

  async submitFIDOLoginForm() {
    WebAuthn.getAssertion(this.state.username.trim())
      .then(() => {
        global.username = this.state.username
        this.props.onPageChange('loginSuccess')
      })
      .catch(err => {
        console.warn('getAssertion', err)
        alert(err)
      })
  }

  render() {
    const {classes} = this.props
    return (
      <div className={classes.container}>
        <form className={classes.form}>
          <TextField
            id="outlined-name"
            label="username"
            className={classes.textField}
            value={this.state.username}
            onChange={event => this.handlerUsernameChange(event)}
            margin="normal"
          />
        </form>
        <MenuItemComponent type={'fido'} onTypeChange={type => this.setState({type: type})}/>
        {
          this.state.type === 'hotp' && <HOTPLoginMenuComponent codeChange={code => this.setState({code: code})}/>
        }
        <div className={classes.buttonWrapper}>
          <Button color={"primary"} variant={"outlined"} className={classes.button}
                  onClick={() => this.submitLoginForm()}>Sign In</Button>
        </div>
        <div className={classes.registerWrapper}>
          <Link href={'javascript:;'} className={classes.tips} onClick={this.props.onLinkClick}>
            REGISTER
          </Link>
        </div>
      </div>
    );
  }
}

const styles = theme => ({
  container: {
    height: '100%',
    marginTop: theme.spacing.unit,
    flexDirection: 'column',
  },
  title: {
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    paddingRight: theme.spacing.unit,
    marginTop: theme.spacing.unit

  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  button: {
    marginTop: 10,
    width: 200
  },
  buttonWrapper: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: theme.spacing.unit
  },
  registerWrapper: {
    marginTop: theme.spacing.unit,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  textField: {
    width: 220,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },

})

export default withStyles(styles)(LoginPage)