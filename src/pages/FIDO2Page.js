import React from 'react'
import withStyles from "@material-ui/core/es/styles/withStyles";
import FIDO2LoginPage from "../components/FIDO2LoginPage";
import FIDO2RegisterComponent from "../components/FIDO2RegisterComponent";


class FIDO2Page extends React.PureComponent {
  constructor() {
    super()
    this.state = {
      page: 'register'
    }
  }

  render() {
    const {classes} = this.props
    return (
      <div className={classes.container}>
        {
          this.state.page === 'login' ? <FIDO2LoginPage onLinkClick={() => this.setState({page: 'register'})}/> : <FIDO2RegisterComponent onLinkClick={() => this.setState({page: 'login'})}/>
        }
      </div>
    )
  }
}

const styles = theme => ({
  container: {
    // borderColor: theme.palette.divider,
    // borderWidth: 1,
    // borderRadius: 2,
    // borderStyle: 'solid',
    // height: '80%',
    marginTop: theme.spacing.unit,
    marginLeft: theme.spacing.unit * 5,
    marginRight: theme.spacing.unit * 5,
  }
})

export default withStyles(styles)(FIDO2Page)