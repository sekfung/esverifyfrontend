import React from 'react'
import withStyles from "@material-ui/core/es/styles/withStyles";
import {Avatar, Button} from "@material-ui/core";
import MenuItemComponent from "../components/MenuItemComponent";
import HOTPRegisterMenuComponent from "../components/HOTPRegisterMenuComponent";
import WebAuthn from "../utils/webauthn";
import global from '../global'
import httpRequest from "../utils/httpRequest";


class LoginSuccessPage extends React.PureComponent {
  constructor() {
    super()
    this.state = {
      type: 'fido',
      username: global.username,
      return_digit: 6,
      secret_key: ""
    }
  }

  logout() {
    this.props.onPageChange('login')
  }

  submitRegForm() {
    if (this.state.type === 'fido') {
      this.submitFIDORegForm()
    }else {
      this.submitHOTPRegForm()
    }
  }

  submitHOTPRegForm() {
    httpRequest.post("/otpserverdemo/hotp/user/register", this.buildOTPForm())
      .then(response => {
        if (response) {
          if (response.errorCode === 0) {
            alert('Success')
            this.props.onPageChange('login')
          }else {
            alert(response.message)
          }
        }
      })
      .catch(err => {
        console.warn(err)
        alert(err)
      })
  }


  buildOTPForm() {
    return JSON.stringify({
      username: this.state.username,
      returnDigit: Number(this.state.return_digit),
      secretKey: this.state.secret_key,
    })
  }

  async submitFIDORegForm() {
    WebAuthn.makeCredential(this.state.username.trim())
      .then(() => {
        this.setState({loggedIn: true})
        alert('Success')
      })
      .catch(err => {
        alert(err)
      })
  }

  render() {
    const {classes} = this.props
    return (
      <div className={classes.avatarWrapper}>
        <Avatar src={'avatar.jpg'} />
        <MenuItemComponent onTypeChange={type => this.setState({type: type})}/>
        {
          this.state.type === 'hotp' && <HOTPRegisterMenuComponent onDigitChange={digit => this.setState({return_digit: digit})} onKeyChange={key => this.setState({secret_key: key})}/>
        }
        <div className={classes.buttonWrapper}>
          <Button color={"primary"} variant={"outlined"} className={classes.button} onClick={() => this.submitRegForm()}>Register New Key</Button>
        </div>
        <Button color={"primary"} variant={"outlined"} className={classes.button} onClick={() => this.logout()}>Log Out</Button>
      </div>
    );
  }

}


const styles = theme => ({
  container: {

  },
  avatarWrapper: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: theme.spacing.unit
  },
  button: {
    marginTop: 20,
    width: 200
  },
})
export default withStyles(styles)(LoginSuccessPage)