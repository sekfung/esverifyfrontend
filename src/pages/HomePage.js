import React from 'react'
import {TableBody, TableCell, TableRow, Typography} from "@material-ui/core";
import withStyles from "@material-ui/core/es/styles/withStyles";

class HomePage extends React.PureComponent {
  constructor() {
    super()
  }

  handleCellClick(index) {
    this.props.onCellClick(index)
  }

  render() {
    const {classes} = this.props
    return (
      <div className={classes.container}>
        <TableBody>
          <TableRow
            key={'fido2'}
            hover
            className={classes.row}
          >
            <img src={'flash-drive.png'} className={classes.img}/>
            <TableCell className={classes.cell} onClick={() => this.handleCellClick('fido2')}>
              <Typography variant='body1'>FIDO</Typography>
            </TableCell>
          </TableRow>
          <TableRow
            key={'hotp'}
            hover
            className={classes.row}
          >
            <img src={'flash-drive.png'} className={classes.img}/>
            <TableCell className={classes.cell} onClick={() => this.handleCellClick('hotp')}>
              <Typography variant='body1'>OATH—HOTP</Typography>
            </TableCell>
          </TableRow>
        </TableBody>
      </div>
    )
  }

}

const styles = theme => ({
  container: {
    marginTop: theme.spacing.unit
  },
  img:{
    marginLeft: theme.spacing.unit,
    marginTop: theme.spacing.unit
  },
  row: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing.unit,
    height: 40,
  },
  cell: {
    width: 300
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
})

export default withStyles(styles)(HomePage)