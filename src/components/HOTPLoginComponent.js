import React from 'react'
import withStyles from "@material-ui/core/es/styles/withStyles";
import {Avatar, Button} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Constants from "../common/Constants";
import Link from "@material-ui/core/Link";


class HOTPLoginComponent extends React.PureComponent {
  constructor() {
    super()
    this.state = {
      username: '',
      code: '',
      isLogin: false,
    }
  }

  handlerUsernameChange(event) {
    this.setState({
      username: event.target.value
    })
  }

  handlerCodeChange(event){
    this.setState({
      code: event.target.value
    })
  }

  buildForm() {
    return JSON.stringify({
      username: this.state.username,
      code: this.state.code
    })
  }

  initHeader() {
    let initHeaders = new Headers()
    initHeaders.append('Accept', 'application/json, text/plain, */*')
    initHeaders.append('Cache-Control', 'no-cache')
    initHeaders.append('Content-Type', 'application/json;charset=UTF-8')
    return initHeaders
  }

  login() {
    fetch(`${Constants.BASE_OTP_URL}/hotp/user/login`, {
      method: 'POST',
      body: this.buildForm(),
      mode: 'cors',
      headers: this.initHeader(),
    })
      .then(response => {
        console.log('response', response)
        return response.json()
      })
      .then(result => {
        if (result && result.errorCode === 0) {
          this.setState({isLogin: true})
        }else {
          alert(result.message)
        }
      })
      .catch(err => {
        console.log('error', err)
        alert(err.toString())
      })
  }

  logout() {
    this.setState({isLogin: false})
  }

  render() {
    const {classes} = this.props
    return (
      <div className={classes.container}>
        {
          !this.state.isLogin ?
            <div className={classes.container}>
              <form  className={classes.form} >
                <TextField
                  id="outlined-name"
                  label="username"
                  className={classes.textField}
                  value={this.state.username}
                  onChange={event => this.handlerUsernameChange(event)}
                  margin="normal"
                  variant="outlined"
                />
                <TextField
                  id="code"
                  label="code"
                  className={classes.textField}
                  value={this.state.code}
                  onChange={event => this.handlerCodeChange(event)}
                  margin="normal"
                  variant="outlined"
                />
              </form>
              <Button color={"primary"} variant={"outlined"} className={classes.button} onClick={() => this.login()}>Sign In</Button>
              <Link href={'javascript:;'} className={classes.tips} onClick={this.props.onLinkClick}>
                Dont't have a account? Or reset account
              </Link>
            </div>
            : (
              <div className={classes.avatarWrapper}>
                <Avatar src={'avatar.jpg'} />
                <Button color={"primary"} variant={"outlined"} className={classes.button} onClick={() => this.logout()}>Log Out</Button>
              </div>
            )
        }

      </div>
    )
  }
}

const styles = theme => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    borderRadius: theme.spacing.unit,
    borderColor: theme.palette.divider
  },
  textField: {
    width: 200,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  form: {
    display: 'flex',
    flexDirection: 'column'
  },
  button: {
    marginTop: 30,
    width: 200
  },
  tips: {
    marginTop: 30
  },
  avatarWrapper: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: theme.spacing.unit
  }
})

export default withStyles(styles)(HOTPLoginComponent)