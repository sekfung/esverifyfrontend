import React from 'react'
import FormControl from "@material-ui/core/FormControl";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import withStyles from "@material-ui/core/es/styles/withStyles";
import TextField from "@material-ui/core/TextField";


class HOTPLoginMenuComponent extends React.PureComponent {

  constructor(props) {
    super(props)
    this.state = {
      code: props.code
    }
  }

  handleCodeChange(event) {
    this.setState({code: event.target.value})
    this.props.codeChange(event.target.value)
  }

  handlerKeyChange(event) {
    this.setState({
      code: event.target.value
    })
  }

  render() {
    const {classes} = this.props
    return (
      <div className={classes.container}>
        <div>
            <TextField
              id="code"
              label="code"
              className={classes.textField}
              value={this.state.code}
              onChange={event => this.handleCodeChange(event)}
              margin="normal"
            />
        </div>
      </div>
    );
  }

}

const styles = theme => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column'
  },
  formControl: {
    display: 'flex',
    flexDirection: 'row',
  },
  group: {
    display: 'flex',
    flexDirection: 'row',
    marginLeft: theme.spacing.unit,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textField: {
    width: 220,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
})

export default  withStyles(styles)(HOTPLoginMenuComponent)