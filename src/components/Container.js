import React from 'react'
import Paper from '@material-ui/core/Paper';
import Button from "@material-ui/core/es/Button/Button";

export default class Container extends React.PureComponent {

  constructor() {
    super()
  }

  componentDidMount() {

  }

  render() {
    return (
      <div style={{flex: 1}}>
        <Paper>
          <Button>Hello World</Button>
        </Paper>
      </div>
    )
  }

}