import React from 'react'
import FormControl from "@material-ui/core/FormControl";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import withStyles from "@material-ui/core/es/styles/withStyles";
import TextField from "@material-ui/core/TextField";


class HOTPRegisterMenuComponent extends React.PureComponent {

  constructor() {
    super()
    this.state = {
      return_digit: '6',
      secret_key: ''
    }
  }

  handleDigitChange(event) {
    this.setState({return_digit: event.target.value})
    this.props.onDigitChange(event.target.value)
  }

  handlerKeyChange(event) {
    this.setState({
      secret_key: event.target.value
    })
    this.props.onKeyChange(event.target.value)

  }

  render() {
    const {classes} = this.props
    return (
      <div className={classes.container}>
        <div>
            <TextField
              id="secret_key"
              label="secret key"
              className={classes.textField}
              value={this.state.secret_key}
              onChange={event => this.handlerKeyChange(event)}
              margin="normal"
            />
          <FormControl component="fieldset" className={classes.formControl}>
            <RadioGroup
              aria-label="Return Digit"
              name="digit"
              className={classes.group}
              value={this.state.return_digit}
              onChange={event => this.handleDigitChange(event)}
            >
              <FormControlLabel value="6" control={<Radio />} label="6 Digit" color="#42a5f5"/>
              <FormControlLabel value="8" control={<Radio/>} label="8 Digit" color={"#42a5f5"}/>
            </RadioGroup>
          </FormControl>
        </div>
      </div>
    );
  }

}

const styles = theme => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column'
  },
  formControl: {
    display: 'flex',
    flexDirection: 'row',
  },
  group: {
    display: 'flex',
    flexDirection: 'row',
    marginLeft: theme.spacing.unit,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textField: {
    width: 220,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
})

export default withStyles(styles)(HOTPRegisterMenuComponent)