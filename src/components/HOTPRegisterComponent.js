import React from 'react'
import withStyles from "@material-ui/core/es/styles/withStyles";
import {Button} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Constants from "../common/Constants";
import FormControl from "@material-ui/core/FormControl";
import RadioGroup from "@material-ui/core/RadioGroup";
import BackIcon from "@material-ui/icons/ArrowBack"
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import Link from "@material-ui/core/Link";


class HOTPRegisterComponent extends React.PureComponent {
  constructor() {
    super()
    this.state = {
      username: '',
      return_digit: '6',
      secret_key: ''
    }
  }

  handlerUsernameChange(event) {
    this.setState({
      username: event.target.value
    })
  }

  handlerKeyChange(event) {
    this.setState({
      secret_key: event.target.value
    })
  }

  buildForm() {
    return JSON.stringify({
      username: this.state.username,
      returnDigit: Number(this.state.return_digit),
      secretKey: this.state.secret_key,
    })
  }

  initHeader() {
    let initHeaders = new Headers()
    initHeaders.append('Accept', 'application/json, text/plain, */*')
    initHeaders.append('Cache-Control', 'no-cache')
    initHeaders.append('Content-Type', 'application/json;charset=UTF-8')
    return initHeaders
  }

  register() {
    fetch(`${Constants.BASE_OTP_URL}/hotp/user/register`, {
      method: 'POST',
      body: this.buildForm(),
      mode: 'cors',
      headers: this.initHeader(),
    })
      .then(response => {
        console.log('response', response)
        return response.json()
      })
      .then(result => {
        if (result && result.errorCode === 0) {
          alert("Success")
          this.setState({isLogin: true})
        } else {
          alert(result.message)
        }
      })
      .catch(err => {
        console.log('error', err)
        alert(err.toString())
      })
  }

  reset() {
    fetch(`${Constants.BASE_OTP_URL}/hotp/user/reset`, {
      method: 'POST',
      body: this.buildForm(),
      mode: 'cors',
      headers: this.initHeader(),
    })
      .then(response => {
        console.log('response', response)
        return response.json()
      })
      .then(result => {
        if (result && result.errorCode === 0) {
          alert("Success")
          this.setState({isLogin: true})
        } else {
          alert(result.message)
        }
      })
      .catch(err => {
        console.log('error', err)
        alert(err.toString())
      })
  }

  handleDigitChange(event) {
    this.setState({return_digit: event.target.value})
  }

  render() {
    const {classes} = this.props
    return (
      <div className={classes.container}>
        <div>

        </div>
        <form className={classes.form}>
          <TextField
            id="outlined-name"
            label="username"
            className={classes.textField}
            value={this.state.username}
            onChange={event => this.handlerUsernameChange(event)}
            margin="normal"
            variant="outlined"
          />
          <TextField
            id="secret_key"
            label="secret key"
            className={classes.textField}
            value={this.state.secret_key}
            onChange={event => this.handlerKeyChange(event)}
            margin="normal"
            variant="outlined"
          />
        </form>
        <div>
          <FormControl component="fieldset" className={classes.formControl}>
            <RadioGroup
              aria-label="Return Digit"
              name="digit"
              className={classes.group}
              value={this.state.return_digit}
              onChange={event => this.handleDigitChange(event)}
            >
              <FormControlLabel value="6" control={<Radio />} label="6 Digit" color="#42a5f5"/>
              <FormControlLabel value="8" control={<Radio/>} label="8 Digit" color={"#42a5f5"}/>
            </RadioGroup>
          </FormControl>
        </div>
        <div className={classes.buttonWrapper}>
          <Button
            color={"primary"}
            className={classes.button}
            variant={"outlined"}
            onClick={() => this.register()}>
            Register
          </Button>
          <Button
            color={"primary"}
            className={classes.button}
            variant={"outlined"}
            onClick={() => this.reset()}>
            Reset
          </Button>
        </div>
        <Link href={'javascript:;'} className={classes.tips} onClick={this.props.onLinkClick}>
          Sign In
        </Link>
      </div>
    )
  }
}

const styles = theme => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    borderRadius: theme.spacing.unit,
    borderColor: theme.palette.divider
  },
  textField: {
    width: 220,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  form: {
    display: 'flex',
    flexDirection: 'column'
  },
  buttonWrapper: {
    marginTop: 30,
    width: 220
  },
  button: {
    width: 100,
    marginLeft: theme.spacing.unit
  },
  tips: {
    marginTop: 30
  },
  formControl: {
    display: 'flex',
    flexDirection: 'row',
  },
  group: {
    display: 'flex',
    flexDirection: 'row',
    marginLeft: theme.spacing.unit
  },
})

export default withStyles(styles)(HOTPRegisterComponent)