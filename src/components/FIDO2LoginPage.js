import React from 'react'
import withStyles from "@material-ui/core/es/styles/withStyles";
import TextField from "@material-ui/core/TextField";
import {Avatar, Button} from "@material-ui/core";
import Link from "@material-ui/core/Link";
import Constants from "../common/Constants";
import base64url from "../utils/base64";
import webauthn from "../utils/webauthn";


class FIDO2LoginPage extends React.PureComponent {

  constructor() {
    super()
    this.state = {
      username: '',
      keyName: '',
      loggedIn: false
    }
  }

  handlerUsernameChange(event) {
    this.setState({
      username: event.target.value
    })
  }

  logout() {
    this.setState({loggedIn: false})
  }

  initHeader() {
    let initHeaders = new Headers()
    initHeaders.append('Accept', 'application/json, text/plain, */*')
    initHeaders.append('Cache-Control', 'no-cache')
    initHeaders.append('Content-Type', 'application/json;charset=UTF-8')
    return initHeaders
  }

  subLoginForm() {
    let data = {
      username: this.state.username,
    }


  }


  render() {
    const {classes} = this.props
    return (
      <div className={classes.container}>
        {
          !this.state.loggedIn ?
            <div>
              <form className={classes.form}>
                <TextField
                  id="outlined-name"
                  label="username"
                  className={classes.textField}
                  value={this.state.username}
                  onChange={event => this.handlerUsernameChange(event)}
                  margin="normal"
                  variant="outlined"
                />
              </form>
              <Button color={"primary"} variant={"outlined"} className={classes.button}
                      onClick={() => this.subLoginForm()}>Sign In</Button>
              <div className={classes.registerWrapper}>
                <Link href={'javascript:;'} className={classes.tips} onClick={this.props.onLinkClick}>
                  Register
                </Link>
              </div>
            </div>
            :
            (
              <div className={classes.avatarWrapper}>
                <Avatar src={'avatar.jpg'}/>
                <Button color={"primary"} variant={"outlined"} className={classes.button} onClick={() => this.logout()}>Log
                  Out</Button>
              </div>
            )
        }
      </div>
    )
  }
}

const styles = theme => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    borderRadius: theme.spacing.unit,
    borderColor: theme.palette.divider,
  },
  tips: {
    marginTop: 30
  },
  form: {
    display: 'flex',
    flexDirection: 'column'
  },
  button: {
    marginTop: 30,
    width: 200
  },
  registerWrapper: {
    marginTop: theme.spacing.unit * 2,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  avatarWrapper: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: theme.spacing.unit
  }
})

export default withStyles(styles)(FIDO2LoginPage)