import React from 'react'
import {TableBody, TableCell, TableRow, Typography} from "@material-ui/core";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import withStyles from "@material-ui/core/es/styles/withStyles";
import FormControl from "@material-ui/core/FormControl";

class MenuItemComponent extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      type: props.type
    }
  }

  static defaultProps = {
    type: 'fido'
  }

  handleTypeChange(value) {
    this.setState({
      type: value
    })
    this.props.onTypeChange(value)
  }

  render() {
    const {classes} = this.props
    return (
      <div className={classes.container}>
        <TableBody className={classes.tbody}>
          <RadioGroup
            aria-label="Type"
            name="digit"
            className={classes.group}
            value={this.state.type}
            onChange={event => this.handleTypeChange(event.target.value)}
          >
            <TableRow
              key={'fido2'}
              hover
              className={classes.row}
            >
              <TableCell className={classes.cell} onClick={() => this.handleTypeChange('fido')}>
                <FormControlLabel value="fido" control={<Radio checked={this.state.type === 'fido'}/>} color="#42a5f5" className={classes.checkbox}/>
                <img src={'flash-drive.png'} className={classes.icon}/>
                <Typography variant='body1'>FIDO</Typography>
              </TableCell>
            </TableRow>
            <TableRow
              key={'hotp'}
              hover
              className={classes.row}
            >
              <TableCell className={classes.cell}  onClick={() => this.handleTypeChange('hotp')}>
                <FormControlLabel value="hotp" control={<Radio checked={this.state.type === 'hotp'}/>} color={"#42a5f5"} className={classes.checkbox}/>
                <img src={'flash-drive.png'} className={classes.icon}/>
                <Typography variant='body1'>OATH—HOTP</Typography>
              </TableCell>
            </TableRow>
          </RadioGroup>
        </TableBody>
      </div>
    )
  }
}

const styles = theme => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
  },
  cell: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  row: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    height: 55,
  },
  checkbox: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingRight: theme.spacing.unit,
    marginRight: 0
  },
  icon: {
    marginRight: theme.spacing.unit
  },
  tbody: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  group: {
    display: 'flex',
    flexDirection: 'column',
    marginLeft: theme.spacing.unit,
  },
})

export default withStyles(styles)(MenuItemComponent)