import React from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";
import TextField from "@material-ui/core/TextField";
import {Avatar, Button} from '@material-ui/core';
import WebAuthn from '../utils/webauthn'
import Link from "@material-ui/core/Link";
import InputBase from "@material-ui/core/InputBase";


class FIDO2RegisterComponent extends React.PureComponent {
  constructor() {
    super()
    this.state = {
      username: '',
      loggedIn: false,
    }
  }

  async submitRegForm() {
    WebAuthn.makeCredential(this.state.username.trim())
      .then(() => {
        this.setState({loggedIn: true})
      })
      .catch(err => {

      })
  }

  async submitNewRegForm() {
    let response = await WebAuthn.checkCredentialIsReister(this.state.username.trim())
    if (response) {
      console.log('submitNewRegForm response: ', response)
    }else {
      await this.submitRegForm()
    }
  }




  handlerUsernameChange(event) {
    this.setState({
      username: event.target.value
    })
  }

  render() {
    const {classes} = this.props
    return (
      <div className={classes.container}>
        {
          !this.state.loggedIn ?
            <div>
              <form  className={classes.form} >
                <InputBase
                  id="outlined-name"
                  label="username"
                  className={classes.textField}
                  value={this.state.username}
                  onChange={event => this.handlerUsernameChange(event)}
                  margin="normal"
                  variant="outlined"
                />
              </form>
              <Button color={"primary"} variant={"outlined"} className={classes.button} onClick={() => this.submitRegForm()}>Register</Button>
              <div className={classes.registerWrapper}>
                <Link href={'javascript:;'} className={classes.tips} onClick={this.props.onLinkClick}>
                  Sign In
                </Link>
              </div>
            </div>
            : <div className={classes.avatarWrapper}>
              <Avatar src={'avatar.jpg'} />
              <Button color={"primary"} variant={"outlined"} className={classes.button} onClick={() => this.submitNewRegForm()}>Add another key</Button>
              <Link href={'javascript:;'} className={classes.tips} onClick={() => this.setState({loggedIn: false})}>Log out</Link>
            </div>
        }
      </div>
    )
  }
}



const styles = theme => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    borderRadius: theme.spacing.unit,
    borderColor: theme.palette.divider
  },
  form: {
    display: 'flex',
    flexDirection: 'column'
  },
  textField: {

  },
  button: {
    marginTop: 30,
    width: 200
  },
  registerWrapper: {
    marginTop: theme.spacing.unit * 2,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  avatarWrapper: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: theme.spacing.unit
  },
  tips: {
    marginTop: 30
  },
})

export default withStyles(styles)(FIDO2RegisterComponent)