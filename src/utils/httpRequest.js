export default class httpRequest {

  static get(param) {
    return new Promise((resolve, reject) => {
      fetch(param, {
        method: 'get',
        headers: {
          "Content-Type": "application/json;charset=UTF-8"
        }
      })
        .then(response => {
          if (response.ok) {
            return response.json()
          } else {
            console.warn(response.status)
          }
        })
        .then(response => {
          resolve(response)
        })
        .catch(err => {
          reject(err)
        })
    })
  }

  static post(url, body) {
    let init = {
      body: body,
      method: 'post',
      headers: {
        "Content-Type": "application/json;charset=UTF-8"
      },
    }
    return new Promise((resolve, reject) => {
      fetch(url, init)
        .then(response => {
          if (response.ok) {
            return response.json()
          } else {
            console.warn(response.status)
            if (response.status === 404) {
              reject('record not found')
            } else {
              reject('internal server error')
            }
          }
        })
        .then(response => {
          resolve(response)
        })
        .catch(err => {
          reject(err)
        })
    })
  }

}