import {toByteArray, fromByteArray} from '../utils/base64'
import httpRequest from "./httpRequest";


export default class webauthn {
  static detectWebAuthnSupport() {
    if (window.PublicKeyCredential === undefined ||
      typeof window.PublicKeyCredential !== "function") {
      let errorMessage = "Oh no! This browser doesn't currently support WebAuthn.";
      if (window.location.protocol === "http:" && (window.location.hostname !== "localhost" && window.location.hostname !== "127.0.0.1")) {
        errorMessage = "WebAuthn only supports secure connections. For testing over HTTP, you can use the origin \"localhost\"."
      }
      throw errorMessage
    }
  }

  static string2buffer(str) {
    return (new Uint8Array(str.length)).map(function (x, i) {
      return str.charCodeAt(i)
    });
  }

// Encode an ArrayBuffer into a base64 string.
  static bufferEncode(value) {
    return fromByteArray(value)
      .replace(/\+/g, "-")
      .replace(/\//g, "_")
      .replace(/=/g, "");
  }

  // Don't drop any blanks
  // decode
  static bufferDecode(value) {
    return Uint8Array.from(atob(value), c => c.charCodeAt(0));
  }

  static buffer2string(buf) {
    let str = "";
    if (!(buf.constructor === Uint8Array)) {
      buf = new Uint8Array(buf);
    }
    buf.map(function (x) {
      return str += String.fromCharCode(x)
    });
    return str;
  }


  static async checkCredentialIsReister(username = "") {
    let that = this;
    let makeAssertionOptions = await httpRequest.get('/fido/assertion/' + username)
    console.log("Assertion Options:");
    console.log(makeAssertionOptions);
    makeAssertionOptions.publicKey.challenge = that.bufferDecode(makeAssertionOptions.publicKey.challenge);
    makeAssertionOptions.publicKey.allowCredentials.forEach(function (listItem) {
      listItem.id = that.bufferDecode(listItem.id)
    });
    console.log(makeAssertionOptions);
    let credential = await navigator.credentials.get({
      publicKey: makeAssertionOptions.publicKey
    })
    console.log(credential);
    let credentials = await that.getCredentials(username);
    console.log('credentials', credentials)
    return credentials.includes(credential)
  }

  static getCredentials(username = "") {
    return httpRequest.post('/fido/credential/' + username, null)
  }

  static async makeCredential(username = "") {
    console.log("Fetching options for new credential");
    let attestation_type = 'none';
    let authenticator_attachment = '';
    let that = this;
    let body = this.jsonToURI({
      attType: attestation_type,
      authType: authenticator_attachment
    })
    try {
      let makeCredentialOptions = await httpRequest.get('/fido/makeCredential/' + username + body)
      makeCredentialOptions.publicKey.challenge = that.bufferDecode(makeCredentialOptions.publicKey.challenge);
      makeCredentialOptions.publicKey.user.id = that.bufferDecode(makeCredentialOptions.publicKey.user.id);
      if (makeCredentialOptions.publicKey.excludeCredentials) {
        for (let i = 0; i < makeCredentialOptions.publicKey.excludeCredentials.length; i++) {
          makeCredentialOptions.publicKey.excludeCredentials[i].id = that.bufferDecode(makeCredentialOptions.publicKey.excludeCredentials[i].id);
        }
      }
      console.log("Credential Creation Options");
      console.log(makeCredentialOptions);
      let newCredential = await navigator.credentials.create({
        publicKey: makeCredentialOptions.publicKey
      })
      console.log("PublicKeyCredential Created");
      console.log(newCredential)
      that.registerNewCredential(newCredential);
    } catch (e) {
      throw e
    }
  }

  // This should be used to verify the auth data with the server
  static registerNewCredential(newCredential) {
    // Move data into Arrays incase it is super long
    let attestationObject = new Uint8Array(newCredential.response.attestationObject);
    let clientDataJSON = new Uint8Array(newCredential.response.clientDataJSON);
    let rawId = new Uint8Array(newCredential.rawId);
    let body = JSON.stringify({
      id: newCredential.id,
      rawId: this.bufferEncode(rawId),
      type: newCredential.type,
      response: {
        attestationObject: this.bufferEncode(attestationObject),
        clientDataJSON: this.bufferEncode(clientDataJSON),
      }
    })
    return httpRequest.post('/fido/makeCredential', body)
  }


  static async getAssertion(username = "") {
    let that = this;
    try {
      let response = await httpRequest.get('/fido/user/' + username + '/exists')
      console.log('getAssertion response:', response)
      let makeAssertionOptions = await httpRequest.get('/fido/assertion/' + username)
      console.log("Assertion Options:");
      console.log(makeAssertionOptions);
      makeAssertionOptions.publicKey.challenge = that.bufferDecode(makeAssertionOptions.publicKey.challenge);
      makeAssertionOptions.publicKey.allowCredentials.forEach(function (listItem) {
        listItem.id = that.bufferDecode(listItem.id)
      });
      console.log(makeAssertionOptions);
      let credential = await navigator.credentials.get({
        publicKey: makeAssertionOptions.publicKey
      })
      console.log(credential);
      await that.verifyAssertion(credential)
    } catch (error) {
      if (!error.exists) {
        throw "User not found, try registering one first!"
      }
      throw error
    }
  }

  static verifyAssertion(assertedCredential) {
    // Move data into Arrays incase it is super long
    console.log('calling verify');
    let authData = new Uint8Array(assertedCredential.response.authenticatorData);
    let clientDataJSON = new Uint8Array(assertedCredential.response.clientDataJSON);
    let rawId = new Uint8Array(assertedCredential.rawId);
    let sig = new Uint8Array(assertedCredential.response.signature);
    let userHandle = new Uint8Array(assertedCredential.response.userHandle);

    let body = JSON.stringify({
      id: assertedCredential.id,
      rawId: this.bufferEncode(rawId),
      type: assertedCredential.type,
      response: {
        authenticatorData: this.bufferEncode(authData),
        clientDataJSON: this.bufferEncode(clientDataJSON),
        signature: this.bufferEncode(sig),
        userHandle: this.bufferEncode(userHandle),
      },
    })
    return httpRequest.post("/fido/assertion", body)

  }

  static jsonToURI(jsonObj) {
    if (!jsonObj) {
      return jsonObj
    }
    let output = '?';
    let keys = Object.keys(jsonObj);
    keys.forEach(function (key) {
      output = output + key + '=' + jsonObj[key] + '&';
    });
    return output.slice(0, -1);
  }
}
